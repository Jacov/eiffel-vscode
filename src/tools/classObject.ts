import { Attribute } from './attribute';
import { Constant } from './constant';
import { Routine } from './routine';

/**
 * Class that represents a class of the Eiffel project
 */
export class ClassObject {
	name: string = "";
	attributes: Attribute[] = [];
	constants: Constant[] = [];
	routines: Routine[] = [];
}