note
	description: "foo class"
	date: "$Date$"
	revision: "$Revision$"

class
	FOO_WITH_ROUTINES_AND_CONSTANTS

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		do
			print ("Foo with routines and constants is working!%N")
		end

feature -- Attributes

    foo_constant_string: STRING = "This is foo's constant!"

    foo_constant_universe_response: INTEGER = 42

    print_foo
        do
            print ("Printing foo with routines and constants!%N")
        end

    add(a_first_number, a_second_number: INTEGER): INTEGER
        do
            result := (a_first_number + a_second_number)
        end

end
