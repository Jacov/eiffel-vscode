note
	description: "foo class"
	date: "$Date$"
	revision: "$Revision$"

class
	FOO_WITH_ROUTINES_AND_ATTRIBUTES

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make(a_foo_name: STRING; a_foo_x, a_foo_y: INTEGER)
		do
            foo_x := a_foo_x
            foo_y := a_foo_y
            foo_name := a_foo_name
			print ("Foo with routines and attributes is working!%N")
        ensure
            foo_x = a_foo_x
            foo_y = a_foo_y
            foo_name = a_foo_name
		end

feature -- Attributes

    foo_name: STRING

    foo_x: INTEGER

    set_foo_x(a_foo_x: INTEGER)
        do
            foo_x := a_foo_x
        ensure
            foo_x = a_foo_x
        end

    foo_y: INTEGER

    set_foo_y(a_foo_y: INTEGER)
        do
            foo_y := a_foo_y
        ensure
            foo_y = a_foo_y
        end

    print_foo
        do
            print ("Printing foo with routines and attributes!%N")
        end

    add(a_first_number, a_second_number: INTEGER): INTEGER
        do
            result := (a_first_number + a_second_number)
        end

end
