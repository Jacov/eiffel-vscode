note
	description: "foo class"
	date: "$Date$"
	revision: "$Revision$"

class
	FOO_WITH_ROUTINES

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		do
			print ("Foo with routines is working!%N")
		end

feature -- Attributes

    print_foo
        do
            print ("Printing foo with routines!%N")
        end

    add(a_first_number, a_second_number: INTEGER): INTEGER
        do
            result := (a_first_number + a_second_number)
        end

end
