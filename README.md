# eiffel-vscode README

<span style="color:red;font-weight:700;font-size:20px">
THE PROJECT IS A BETA TESTING AND NOT 100% FUNCTIONNAL.  You can use it, but it's at your own risks.</span>

## General informations

- The project is a Visual Studio Code language support plug-in for the Eiffel language.
- Created by Jacov Montminy, January 2023
- Last update : March 2023
- Version 1.0

## Questions and/or comments?

Join our discord server at : https://discord.gg/KXqsQHyDpq .


## License (MIT)

You are free to use this in any way you want, in case you find this useful or working for you but you must keep the copyright notice and license. (MIT)

## Features

- Base highlighting for *Eiffel* keywords
- Base automatic indentation
- Base snippets implemented for **if**, **while**, **for**, etc (many more needs to be implemented)
- Base auto-completion for class identifiers (**need further improvements**)
- Compile and run your project with the ```Compile and run``` command (from the command palette). You can access the command palette with the default keybind ```Ctrl+Shift+P```.
  ![image-20230302102018479](C:\Users\Jacov\AppData\Roaming\Typora\typora-user-images\image-20230302102018479.png)
- Index the projet (for auto-completion) with the ```Index project``` command (from the command palette). You can access the command palette with the default keybind ```Ctrl+Shift+P```.![image-20230302120138669](C:\Users\Jacov\AppData\Roaming\Typora\typora-user-images\image-20230302120138669.png)

## Requirements

- Make sure you have Visual Studio Code installed on your computer. You can download it on the [official website](https://code.visualstudio.com/Download).

- Make sure you have Eiffel Studio (to have the Eiffel compiler) installed on your computer. You can download it on the [official Eiffel website](https://account.eiffel.com/downloads). This extension was based on the 19.05 version of Eiffel Studio. If you want to make sure it's working properly, we recommand using the same version.

- Make sure you have those three environment variables set properly :

  - ```EIFFEL_VSCODE_EC_PATH``` : Create and assign this variable to the Eiffel compiler path (make sure you're referencing ```ec.exe``` wich supports graphic interfaces, **not** ```ecb.exe```). Example : ```C:\Program Files\Eiffel Software\EiffelStudio 19.05 GPL\studio\spec\win64\bin\ec.exe```.

  - ```EIFFEL_VSCODE_PROJECT_DIR_PATH``` : Create and assign this variable to your Eiffel project directory path. Example : ```C:\MyProjects\MyAwsomeEiffelProjectDirectory```.

  - ```EIFFEL_VSCODE_PROJECT_FILE_NAME``` : Create and assign this variable to your Eiffel project file name. Example : ```my_awesome_eiffel_project.ecf```.

## Extension Settings

<span style="color:red;font-weight:700;">
There are no extension settings yet, but keep this section as it is for further settings.
</span>

Include if your extension adds any VS Code settings through the `contributes.configuration` extension point.

For example:

This extension contributes the following settings:

* `myExtension.enable`: Enable/disable this extension.
* `myExtension.thing`: Set to `blah` to do something.

## Known Issues

- Need to find a faster solution to retrieve class identifiers from the compiler. Right now, we're using separated process for each command, which is way more slower than if we had a single process running in background and retrieving data.

## Release Notes

### 1.0.0

No release yet, the project is still in beta testing. You can use it, but it's at your own risks.

---

## Following extension guidelines

Ensure that you've read through the extensions guidelines and follow the best practices for creating your extension.

* [Extension Guidelines](https://code.visualstudio.com/api/references/extension-guidelines)

## Development

- [Make sure you have the requirements](#Requirements) (Eiffel compiler and environment variables).
- Create or use an existent GitLab account.
- Clone the repository : [gitlab.com/Eiffel-vscode](https://gitlab.com/Jacov/Eiffel-vscode)
- Open the project in Visual Studio Code.
- Open a terminal in the root directory and run the command line ```npm install``` to get all the dependencies.
- Make sure to read and understand the language standards used for development before coding. Standards  are described in the **DEV_STANDARDS.md** file, at the root of the project. You are welcome to give your advices (use the [discord link](#Questions and/or comments?) mentioned up above) about the standards if you think that they are lacking something.
- **IMPORTANT** : if you want to run the extension tests locally, make sure you :
  - Have a new environment variable ```EIFFEL_VSCODE_TEST_CWD_PATH``` set with the path of the extension test project. Example : ```C:\MyFolder\eiffel-vscode\src\test\test_project```.

- Start coding!

## Credits

### Syntax
The original file ***Eiffel.tmLanguage*** has been created by ***Thomas Boutin***. It is licensed under ***MIT***.

GitHub : https://github.com/Thomas-Boutin/Eiffel-Language, **01-02-2023**

### Logo of the project
The original file ***eiffel_logo.png*** has been created by ***Alexander (Sasha)***. It is licensed under ***CC-BY-SA-4.0***.

Image picked on : https://commons.wikimedia.org/wiki/File:Eiffel_logo.svg, **08-02-2023**
